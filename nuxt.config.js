//----------------------------------------------------------------------------------------------------------------------
// Nuxt Config
//----------------------------------------------------------------------------------------------------------------------

const hooks = require('./lib/hooks');

//----------------------------------------------------------------------------------------------------------------------

module.exports = {
    head: {
        title: 'Fate and the Fablemaidens',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
            { hid: 'description', name: 'description', content: 'Fate and the Fablemaidens is a Dungeons & Dragons 5th edition actual-play podcast by three friends who love goofs and games.'},
            { hid: 'keywords', name: 'keywords', content: 'dnd, family-friendly, all-female, humor, podcast, children, story-telling, comedy, dungeons and dragons, show, friendship, steampunk, magitek, actual-play, fate, community, fablemaidens, goofs, ttrpg, tabletop gaming' },
            { hid: 'robots', name: 'robots', content: 'index, follow' },
            { hid: 'revisit-after', name: 'revisit-after', content: '1 month'}
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Chivo%7CForum%7CLimelight'  },
            { rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', integrity: "sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN", crossorigin:"anonymous" }
        ]
    },
    css: [ { src: '~assets/scss/main.scss', lang: 'scss' } ],
    modules: [
        'bootstrap-vue/nuxt'
    ],
    hooks: {
        build: {
            async before()
            {
                console.log('i dont care what you say, bitch');
                return Promise.all([hooks.generatePlaylist(), hooks.generateFeed()])
                    .then(() => console.log('you dub'));
            }
        }
    },
    plugins: [
        { src: '~/plugins/facebook-sdk.js', mode: 'client' },
        { src: '~/plugins/vue-aplayer.js', mode: 'client'}
    ],
    build: {
        extend(config, { isDev, isClient })
        {
            config.externals = 'hls.js'
        }
    }
}; // end module.exports

//----------------------------------------------------------------------------------------------------------------------

