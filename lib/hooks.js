//----------------------------------------------------------------------------------------------------------------------
// hooks
//----------------------------------------------------------------------------------------------------------------------

const _ = require('lodash');

const fs = require('fs');
const { promisify } = require('util');
const Podcast = require('podcast');

const awsS3 = require('./awsS3');

//----------------------------------------------------------------------------------------------------------------------

const writeFile = promisify(fs.writeFile);

//----------------------------------------------------------------------------------------------------------------------

function generatePlaylist()
{
    return awsS3.listEpisodes()
        .then((episodes) =>
        {
            const playlist = _.chain(episodes)
                .orderBy('date', ['asc'])
                .map((episode) =>
                {
                    return {
                        title: episode.title,
                        author: 'Fablemaiden Cast',
                        url: episode.url,
                        pic: '/images/iTunesCover.jpg'
                    }
                })
                .value();

            return writeFile('./static/playlist.json', JSON.stringify(playlist, null, 2));
        });
} // end generatePlaylist

function generateFeed()
{
    return awsS3.listEpisodes()
        .then((episodes) =>
        {
            const items = _.chain(episodes)
                .orderBy('date', ['asc'])
                .map((episode) =>
                {
                    return {
                        title: episode.title,
                        description: episode.desc,
                        url: episode.url,
                        date: episode.date,
                        enclosure:
                            {
                                url: `https://dts.podtrac.com/redirect.mp3/${ episode.url }`,
                                size: episode.size,
                                type: episode.type
                            }
                    }
                })
                .value();

            const feed = new Podcast({
                title: "Fate and the Fablemaidens",
                description: "Fate and the Fablemaidens is an actual-play D&D 5th edition podcast created by four friends who love goofs and games",
                feedUrl: "https://fatefmcast.com/feed.xml",
                siteUrl: "https://fatefmcast.com",
                imageUrl: "https://fatefmcast.com/images/iTunesCover.jpg",
                author: "Fablemaiden Cast",
                managingEditor: "Laura Hutton",
                webMaster: "Laura Hutton",
                language: 'en',
                copyright: '2018-2019 Tweedle Creative LLC. All rights reserved.',
                categories: [
                    "Comedy",
                    "Games & Hobbies"
                ],
                pubDate: new Date(),
                ttl: 24 * 60,
                itunesAuthor: "Fablemaiden Cast",
                itunesOwner: {
                    name: "Laura Hutton",
                    email: "contact@fatefmcast.com"
                },
                itunesExplicit: false,
                itunesCategory: [
                    { text: "Comedy" },
                    { text: "Games & Hobbies" }
                ],
                itunesImage: "https://fatefmcast.com/images/iTunesCover.jpg"
            });


            _.each(items, (item) =>
            {
                feed.addItem(item);
            });

            return writeFile('./static/feed.xml', feed.buildXml());
        });
} // end generateFeed

//----------------------------------------------------------------------------------------------------------------------

module.exports = { generatePlaylist, generateFeed };

//----------------------------------------------------------------------------------------------------------------------
