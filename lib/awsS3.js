//----------------------------------------------------------------------------------------------------------------------
// awsS3
//----------------------------------------------------------------------------------------------------------------------

const { promisify } = require('util');
const S3 = require('aws-sdk/clients/s3');
const s3 = new S3({
    region: 'us-east-2',
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

// S3 Operations
const listObjects = promisify(s3.listObjectsV2.bind(s3));
const headObject = promisify(s3.headObject.bind(s3));

// Randos
const bucket = 'fatefmcast-audio';
const prefix = 'episodes';

//----------------------------------------------------------------------------------------------------------------------

function listEpisodes()
{
    const params = {
        Bucket: bucket,
        Prefix: prefix
    };

    return listObjects(params)
        .then(({ Contents }) =>
        {
            const episodes = Contents.map((data) =>
            {
                return headObject({ Bucket: bucket, Key: data.Key })
                    .then((episode) =>
                    {
                        return {
                            Key: data.Key,
                            url: `http://${ bucket }.s3-website.us-east-2.amazonaws.com/${ data.Key }`,
                            ...episode
                        };
                    })
            });

            return Promise.all(episodes)
                .then((episodes) =>
                {
                    return episodes.map((episode) =>
                    {
                        return {
                            key: episode.Key,
                            url: episode.url,
                            date: new Date(episode.Metadata.date),
                            title: episode.Metadata.title,
                            desc: episode.Metadata.desc,
                            type: episode.ContentType,
                            size: episode.ContentLength
                        };
                    });
                });
        });
} // end listEpisodes

//----------------------------------------------------------------------------------------------------------------------

module.exports = { listEpisodes };

//----------------------------------------------------------------------------------------------------------------------
